import { VuexModule, Module, Mutation, Action } from 'vuex-module-decorators';
import { CityI, ContinentI, CountryI, DataToUpload } from '../typings';
import {
  deleteDataFromStore,
  searchItemByInput,
  updateDataInStore,
} from '../utils/utils';

@Module({ namespaced: true })
class Continents extends VuexModule {
  private continents: Array<ContinentI> = [];

  get continentById() {
    return (id: string): ContinentI | undefined => {
      return this.continents.find((continent) => continent.id === id);
    };
  }

  get continentsList(): Array<ContinentI> {
    return this.continents;
  }

  get continentNameById() {
    return (id: string): string => {
      const continent = this.continents.find(
        (continent) => continent.id === id
      );
      if (continent) {
        return continent.name;
      } else {
        return 'no continent';
      }
    };
  }

  get continentsListFiltered() {
    return (searchText: string): CityI[] | ContinentI[] | CountryI[] => {
      return searchItemByInput(searchText, 'continents', this);
    };
  }

  @Mutation
  loadContinents(continents: Array<ContinentI>): void {
    this.continents = continents;
  }

  @Mutation
  loadContinentsWithoutOverwriting(continents: Array<ContinentI>): void {
    const continentsToAdd = continents.filter(
      (continentToAdd) =>
        !this.continents.some((continent) => continent.id === continentToAdd.id)
    );
    this.continents = [...this.continents, ...continentsToAdd];
  }

  @Mutation
  addContinentToStore(continent: ContinentI): void {
    this.continents.push(continent);
  }

  @Mutation
  updateContinentInStore(continent: ContinentI): void {
    updateDataInStore(continent, 'continents', this);
  }

  @Mutation
  deleteContinentFromStore(id: string): void {
    deleteDataFromStore(id, 'continents', this);
  }

  @Action
  fetchContinents(): void {
    if (localStorage.getItem('continents')) {
      const continents: Array<ContinentI> = JSON.parse(
        localStorage.getItem('continents') || ''
      );
      this.context.commit('loadContinents', continents);
    }
  }

  @Action
  uploadContinents(payload: DataToUpload): void {
    payload.overwrite
      ? this.context.commit('loadContinents', payload.data)
      : this.context.commit('loadContinentsWithoutOverwriting', payload.data);
    this.context.dispatch('updateLocalStorageContinents');
  }

  @Action
  checkIfContinentsAreFetched(): void {
    const storeContinents = this.context.getters['continentsList'];
    if (storeContinents.length === 0) {
      this.context.dispatch('fetchContinents');
    }
  }

  @Action
  updateLocalStorageContinents(): void {
    const updatedStoreContinents = this.context.getters['continentsList'];
    const continentsJson = JSON.stringify(updatedStoreContinents);
    localStorage.setItem('continents', continentsJson);
  }

  @Action
  deleteContinent(id: string): void {
    this.context.dispatch('checkIfContinentsAreFetched');
    this.context.commit('deleteContinentFromStore', id);
    this.context.dispatch('updateLocalStorageContinents');
  }

  @Action
  postContinent(continent: ContinentI): void {
    this.context.dispatch('checkIfContinentsAreFetched');
    this.context.commit('addContinentToStore', continent);
    this.context.dispatch('updateLocalStorageContinents');
  }

  @Action
  updateContinent(continent: ContinentI): void {
    this.context.dispatch('checkIfContinentsAreFetched');
    this.context.commit('updateContinentInStore', continent);
    this.context.dispatch('updateLocalStorageContinents');
  }
}

export default Continents;
