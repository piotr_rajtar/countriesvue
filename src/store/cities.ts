import { getCollectionByReference } from '@/utils/utils';
import { VuexModule, Module, Action, Mutation } from 'vuex-module-decorators';
import {
  CityI,
  ContinentI,
  CountryI,
  DataToUpload,
  Coordinates,
} from '../typings';
import {
  deleteDataFromStore,
  searchItemByInput,
  updateDataInStore,
} from '../utils/utils';

@Module({ namespaced: true })
class Cities extends VuexModule {
  cities: CityI[] = [];

  get citiesData(): CityI[] {
    return this.cities;
  }

  get citiesFromCountry() {
    return (id: string): CityI[] => {
      return this.cities.filter(
        (city) => city.countryId.toLocaleLowerCase() === id.toLocaleLowerCase()
      );
    };
  }

  get cityById() {
    return (id: string): CityI | undefined => {
      return this.cities.find((city) => city.id === id);
    };
  }

  get cityNameById() {
    return (id: string): string => {
      const city = this.cities.find((city) => city.id === id);
      if (city) {
        return city.name;
      } else {
        return 'no city';
      }
    };
  }

  get citiesByCountries() {
    return (id: string[]): CityI[] | CountryI[] => {
      return getCollectionByReference(id, 'cities', this, 'countryId');
    };
  }

  get citiesListFiltered() {
    return (searchText: string): CityI[] | ContinentI[] | CountryI[] => {
      return searchItemByInput(searchText, 'cities', this);
    };
  }

  get citiesCoordinates(): Coordinates[] {
    return this.cities.map((city) => {
      return {
        coordinates: {
          lat: city.latitude as number,
          lng: city.longitude as number,
        },
        data: {
          cityId: city.id,
        },
      };
    });
  }

  get citiesCoordinatesById() {
    return (id: string): Coordinates => {
      const city = this.cities.find((city) => city.id === id);
      return {
        coordinates: {
          lat: city!.latitude as number,
          lng: city!.longitude as number,
        },
        data: {
          cityId: city!.id,
        },
      };
    };
  }

  @Mutation
  loadCities(cities: CityI[]): void {
    this.cities = cities;
  }

  @Mutation
  loadCitiesWithoutOverwriting(cities: Array<CityI>): void {
    const citiesToAdd = cities.filter(
      (cityToAdd) => !this.cities.some((city) => city.id === cityToAdd.id)
    );
    this.cities = [...this.cities, ...citiesToAdd];
  }

  @Action
  fetchCities(): void {
    if (localStorage.getItem('cities')) {
      const cities: CityI[] = JSON.parse(localStorage.getItem('cities') || '');
      this.context.commit('loadCities', cities);
    }
  }

  @Action
  uploadCities(payload: DataToUpload): void {
    payload.overwrite
      ? this.context.commit('loadCities', payload.data)
      : this.context.commit('loadCitiesWithoutOverwriting', payload.data);
    this.context.dispatch('updateLocalStorageCities');
  }

  @Mutation
  addCityToStore(city: CityI): void {
    const citiesCopy = this.cities.map((obj) => {
      return { ...obj };
    });
    citiesCopy.push(city);
    this.cities = citiesCopy;
  }

  @Mutation
  updateCityInStore(city: CityI): void {
    updateDataInStore(city, 'cities', this);
  }

  @Mutation
  deleteCityFromStore(id: string): void {
    deleteDataFromStore(id, 'cities', this);
  }

  @Mutation
  overrideCapitalCityInStore(countryId: string): void {
    const cityIndex = this.cities.findIndex(
      (city) => city.countryId === countryId && city.isCapital
    );
    if (cityIndex >= 0) {
      const citiesCopy = this.cities.map((obj) => {
        return { ...obj };
      });
      citiesCopy[cityIndex].isCapital = false;
      this.cities = citiesCopy;
    }
  }

  @Action
  checkIfCitiesAreFetched(): void {
    const storeCities = this.context.getters['citiesData'];
    if (storeCities.length === 0) {
      this.context.dispatch('fetchCities');
    }
  }

  @Action
  updateLocalStorageCities(): void {
    const updatedStoreCities = this.context.getters['citiesData'];
    const citiesJson = JSON.stringify(updatedStoreCities);
    localStorage.setItem('cities', citiesJson);
  }

  @Action
  postCity(payload: { city: CityI; isOverridingCapital: boolean }): void {
    this.context.dispatch('checkIfCitiesAreFetched');
    if (payload.isOverridingCapital) {
      this.context.commit('overrideCapitalCityInStore', payload.city.countryId);
    }
    this.context.commit('addCityToStore', payload.city);
    this.context.dispatch('updateLocalStorageCities');
  }

  @Action
  updateCity(payload: { city: CityI; isOverridingCapital: boolean }): void {
    this.context.dispatch('checkIfCitiesAreFetched');
    if (payload.isOverridingCapital) {
      this.context.commit('overrideCapitalCityInStore', payload.city.countryId);
    }
    this.context.commit('updateCityInStore', payload.city);
    this.context.dispatch('updateLocalStorageCities');
  }

  @Action
  deleteCity(id: string): void {
    this.context.dispatch('checkIfCitiesAreFetched');
    this.context.commit('deleteCityFromStore', id);
    this.context.dispatch('updateLocalStorageCities');
  }
}

export default Cities;
