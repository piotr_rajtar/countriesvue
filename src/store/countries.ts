import { getCollectionByReference } from '@/utils/utils';
import { VuexModule, Module, Action, Mutation } from 'vuex-module-decorators';
import {
  CountryI,
  CountryFormData,
  ContinentI,
  CityI,
  DataToUpload,
} from '../typings';
import {
  deleteDataFromStore,
  searchItemByInput,
  updateDataInStore,
} from '../utils/utils';

@Module({ namespaced: true })
class Countries extends VuexModule {
  private countries: CountryI[] = [];

  get countriesList(): CountryI[] {
    return this.countries;
  }

  get countryById() {
    return (id: string): CountryI | undefined => {
      return this.countries.find((country) => country.id === id);
    };
  }

  get countriesByContinents() {
    return (id: string[]): CountryI[] | CityI[] => {
      return getCollectionByReference(id, 'countries', this, 'continent');
    };
  }

  get countryNameById() {
    return (id: string): string => {
      const country = this.countries.find((country) => country.id === id);
      if (country) {
        return country.name;
      } else {
        return 'no country';
      }
    };
  }

  get countriesFromContinent() {
    return (id: string): CountryI[] => {
      return this.countries.filter(
        (country) =>
          country.continent.toLocaleLowerCase() === id.toLocaleLowerCase()
      );
    };
  }

  get countriesFiltered() {
    return (searchText: string): CityI[] | ContinentI[] | CountryI[] => {
      return searchItemByInput(searchText, 'countries', this);
    };
  }

  @Mutation
  loadCountries(countries: CountryI[]): void {
    this.countries = countries;
  }

  @Mutation
  loadCountriesWithoutOverwriting(countries: Array<CountryI>): void {
    const countriesToAdd = countries.filter(
      (countryToAdd) =>
        !this.countries.some((country) => country.id === countryToAdd.id)
    );
    this.countries = [...this.countries, ...countriesToAdd];
  }

  @Mutation
  deleteContinent(id: string): void {
    this.countries
      .filter((country) => country.continent === id)
      .map((country) => (country.continent = ''));
  }

  @Mutation
  updateCountryInStore(country: CountryI): void {
    updateDataInStore(country, 'countries', this);
  }

  @Mutation
  deleteCountryFromStore(id: string): void {
    deleteDataFromStore(id, 'countries', this);
  }

  @Mutation
  addCountry(country: CountryI): void {
    this.countries.push(country);
  }

  @Mutation
  deleteReference(id: string): void {
    const country = this.countries.find((country) => country.capital === id);
    if (country && country.capital) {
      country.capital = '';
    }
  }

  @Action
  addCountryToLs(country: CountryFormData): void {
    this.context.commit('addCountry', country);
    const countriesToLs = JSON.stringify(this.countries);
    localStorage.setItem('countries', countriesToLs);
  }

  @Action
  checkIfCountriesAreFetched(): void {
    const storeCountries = this.context.getters['countriesList'];
    if (storeCountries.length === 0) {
      this.context.dispatch('fetchCountries');
    }
  }

  @Action
  updateLocalStorageCountries(): void {
    const updatedStoreCountries = this.context.getters['countriesList'];
    const countriesJson = JSON.stringify(updatedStoreCountries);
    localStorage.setItem('countries', countriesJson);
  }

  @Action
  fetchCountries(): void {
    if (localStorage.getItem('countries')) {
      const countries: CountryI[] = JSON.parse(
        localStorage.getItem('countries') || ''
      );
      this.context.commit('loadCountries', countries);
    }
  }

  @Action
  uploadCountries(payload: DataToUpload): void {
    payload.overwrite
      ? this.context.commit('loadCountries', payload.data)
      : this.context.commit('loadCountriesWithoutOverwriting', payload.data);
    this.context.dispatch('updateLocalStorageCountries');
  }

  @Action
  deleteCountry(id: string): void {
    this.context.dispatch('checkIfCountriesAreFetched');
    this.context.commit('deleteCountryFromStore', id);
    this.context.dispatch('updateLocalStorageCountries');
  }

  @Action
  postCountry(country: CountryI): void {
    this.context.dispatch('checkIfCountriesAreFetched');
    this.context.commit('addCountry', country);
    this.context.dispatch('updateLocalStorageCountries');
  }

  @Action
  updateCountry(country: CountryI): void {
    this.context.dispatch('checkIfCountriesAreFetched');
    this.context.commit('updateCountryInStore', country);
    this.context.dispatch('updateLocalStorageCountries');
  }

  @Action
  deleteCountryReference(id: string): void {
    this.context.dispatch('checkIfCountriesAreFetched');
    this.context.commit('deleteReference', id);
    this.context.dispatch('updateLocalStorageCountries');
  }

  @Action
  deleteContinentReference(id: string): void {
    this.context.dispatch('checkIfCountriesAreFetched');
    this.context.commit('deleteContinent', id);
    this.context.dispatch('updateLocalStorageCountries');
  }
}
export default Countries;
