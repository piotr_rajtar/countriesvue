import Vue from 'vue';
import Vuex from 'vuex';

import Countries from './countries';
import Continents from './continents';
import Cities from './cities';

import { loadDataToLocalStorage } from '../utils/utils';

Vue.use(Vuex);

export default new Vuex.Store({
  actions: {
    loadDataToLocalStorage() {
      if (!localStorage.getItem('continents')) {
        loadDataToLocalStorage();
      }
    },
  },
  modules: {
    countries: Countries,
    continents: Continents,
    cities: Cities,
  },
});
