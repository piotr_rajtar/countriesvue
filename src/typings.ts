/* eslint-disable @typescript-eslint/no-empty-interface */

export interface ContinentDetailsI {
  name: string;
  area: number;
  population: number;
  populationDensity: number;
  minHeight: number;
  maxHeight: number;
  image?: string;
}
export interface ContinentI extends ContinentDetailsI {
  id: string;
  entity: string;
}

export interface ContinentFormData {
  id: string;
  name: string;
  area: number | null;
  population: number | null;
  populationDensity: number | null;
  minHeight: number | null;
  maxHeight: number | null;
  image?: string;
}

export interface CountryDetailsI {
  name: string;
  capital?: string;
  area: number;
  population: number;
  populationDensity: number;
  image?: string;
}

export interface CountryI extends CountryDetailsI {
  id: string;
  continent: string;
  entity: string;
}

export interface CountryFormData {
  id: string;
  name: string;
  continent: string;
  capital?: string;
  area: number | null;
  population: number | null;
  populationDensity: number | null;
  image?: string;
}

export interface CityDetailsI {
  name: string;
  area: number;
  population: number;
  populationDensity: number;
  isCapital: boolean;
  image?: string;
  longitude?: number;
  latitude?: number;
}

export interface CityI extends CityDetailsI {
  id: string;
  entity: string;
  countryId: string;
}

export interface CityFormData {
  id: string;
  name: string;
  countryId: string;
  area: number | null;
  population: number | null;
  populationDensity: number | null;
  isCapital: boolean;
  image?: string;
}

export interface Coordinates {
  coordinates: {
    lat: number;
    lng: number;
  };
  data: {
    cityId: string;
  };
}

export interface SelfI {}

export interface CSVFileFormat extends ContinentI, CountryI, CityI {
  entity: 'continent' | 'country' | 'city';
}

export interface DataToUpload {
  data: ContinentI[] | CountryI[] | CityI[];
  overwrite: boolean;
}
