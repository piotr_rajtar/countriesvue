import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import Continents from '../containers/views/Continents.vue';
import Continent from '../containers/views/Continent.vue';
import ContinentForm from '../containers/UI/form/ContinentForm.vue';
import Countries from '../containers/views/Countries.vue';
import Country from '../containers/views/Country.vue';
import CountryForm from '../containers/UI/form/CountryForm.vue';
import Cities from '../containers/views/Cities.vue';
import City from '../containers/views/City.vue';
import CityForm from '../containers/UI/form/CityForm.vue';
import AdminPanel from '../components/views/AdminPanel.vue';
import Map from '../containers/views/Map.vue';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  { path: '/', redirect: '/continents' },
  {
    path: '/continents',
    component: {
      render(c) {
        return c('router-view');
      },
    },
    children: [
      {
        path: '',
        name: 'continents',
        component: Continents,
      },
      {
        path: 'create',
        name: 'continent-create',
        component: ContinentForm,
      },
      {
        path: ':continentId',
        component: {
          render(c) {
            return c('router-view');
          },
        },
        children: [
          {
            path: '',
            name: 'continent',
            component: Continent,
            props: true,
          },
          {
            path: 'edit',
            name: 'continent-edit',
            component: ContinentForm,
            props: true,
          },
        ],
      },
    ],
  },
  {
    path: '/countries',
    component: {
      render(c) {
        return c('router-view');
      },
    },
    children: [
      {
        path: '',
        name: 'countries',
        component: Countries,
      },
      {
        path: 'create',
        name: 'country-create',
        component: CountryForm,
      },
      {
        path: ':countryId',
        component: {
          render(c) {
            return c('router-view');
          },
        },
        children: [
          {
            path: '',
            name: 'country',
            component: Country,
            props: true,
          },
          {
            path: 'edit',
            name: 'country-edit',
            component: CountryForm,
            props: true,
          },
        ],
      },
    ],
  },
  {
    path: '/cities',
    component: {
      render(c) {
        return c('router-view');
      },
    },
    children: [
      {
        path: '',
        name: 'cities',
        component: Cities,
      },
      {
        path: 'create',
        name: 'city-create',
        component: CityForm,
      },
      {
        path: ':cityId',
        component: {
          render(c) {
            return c('router-view');
          },
        },
        children: [
          {
            path: '',
            name: 'city',
            component: City,
            props: true,
          },
          {
            path: 'edit',
            name: 'city-edit',
            component: CityForm,
            props: true,
          },
        ],
      },
    ],
  },
  {
    path: '/admin',
    name: 'admin',
    component: AdminPanel,
  },
  {
    path: '/map',
    name: 'map',
    component: Map,
  },
];

const router = new VueRouter({
  routes,
  mode: 'history',
});

export default router;
