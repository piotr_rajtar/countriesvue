export const mapFilters: Array<string> = ['multiselect', 'distance'];
export const defaultCircleRange: number = 0;
export const equatorLengthKm: number = 40075;

export enum MapObjects {
  MARKER = 'marker',
  ALL = 'all',
}
