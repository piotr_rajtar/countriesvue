import { CSVFileFormat } from '../../typings';

class InterfacePropsHelper implements CSVFileFormat {
  entity: 'continent' | 'country' | 'city' = 'continent';
  id: string = '';
  name: string = '';
  area: number = 0;
  population: number = 0;
  populationDensity: number = 0;
  minHeight: number = 0;
  maxHeight: number = 0;
  image?: string | undefined = '';
  continent: string = '';
  capital?: string | undefined = '';
  countryId: string = '';
  isCapital: boolean = false;
  longitude?: number | undefined = 0;
  latitude?: number | undefined = 0;
}

export const CSVFileFormatKeys = Object.keys(new InterfacePropsHelper());
