import { countries, continents, cities } from '../initialData';
import { CityI, ContinentI, CountryI, SelfI } from '../typings';

export function loadDataToLocalStorage(): void {
  localStorage.setItem('countries', JSON.stringify(countries));
  localStorage.setItem('continents', JSON.stringify(continents));
  localStorage.setItem('cities', JSON.stringify(cities));
}
export function getCollectionByReference(
  id: string[],
  stateName: string,
  self: SelfI,
  prop: string
): CountryI[] | CityI[] {
  const resultArray = id.map((e) => {
    const itemArr = self[stateName].filter(
      (item: ContinentI[] | CountryI[]) => item[prop] === e
    );
    return itemArr.filter(
      (item: ContinentI[] | CountryI[]) => item[prop] === e
    );
  });
  const flattened = ([] as []).concat(...resultArray);
  return flattened;
}

export function deleteDataFromStore(
  id: string,
  stateName: string,
  self: SelfI
): void {
  const itemIndex = self[stateName].findIndex(
    (item: ContinentI | CountryI | CityI) => item.id === id
  );
  if (itemIndex >= 0) {
    const stateCopy = self[stateName].map(
      (obj: ContinentI | CountryI | CityI) => {
        return { ...obj };
      }
    );
    stateCopy.splice(itemIndex, 1);
    self[stateName] = stateCopy;
  } else {
    throw new Error("Provided id doesn't exist");
  }
}

export function updateDataInStore(
  data: ContinentI | CountryI | CityI,
  stateName: string,
  self: SelfI
): void {
  const itemIndex = self[stateName].findIndex(
    (i: ContinentI | CountryI | CityI) => i.id === data.id
  );
  if (itemIndex >= 0) {
    const stateCopy = self[stateName].map(
      (obj: ContinentI | CountryI | CityI) => {
        return { ...obj };
      }
    );
    stateCopy.splice(itemIndex, 1, data);
    self[stateName] = stateCopy;
  } else {
    throw new Error("Provided id doesn't exist");
  }
}

export function searchItemByInput(
  searchText: string,
  stateName: string,
  self: SelfI
): ContinentI[] | CountryI[] | CityI[] {
  return self[stateName].filter((item: ContinentI | CountryI | CityI) =>
    item.name.toLowerCase().includes(searchText.toLowerCase())
  );
}
