import BaseButton from '../components/UI/BaseButton.vue';

export default {
  title: 'BaseButton',
  component: BaseButton,
  argTypes: {
    mode: {
      control: {
        type: 'radio',
        options: ['primary', 'secondary', 'deafult'],
      },
    },
    text: {
      control: {
        type: 'text',
      },
    },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: {
    BaseButton,
  },
  template: `<BaseButton v-bind="$props">{{text ? text : 'Send'}}</BaseButton>`,
});

export const Primary = Template.bind({});
Primary.args = {
  mode: 'primary',
};

export const Secondary = Template.bind({});
Secondary.args = {
  mode: 'secondary',
};

export const Default = Template.bind({});
Default.args = {
  mode: 'default',
};
