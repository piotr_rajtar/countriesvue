# STYLE GUIDE
This project is using SASS with SCSS syntax.

## BASIC RULES
### Tabs & spaces
For indentation and formatting of the code, we use spaces and NOT tabs. 
Spaces are the only way to guarantee code renders the same in any person’s environment.

### Avoid undoing/overriding
You’re undoing CSS when you are writing declarations that override other declarations written somewhere else in the code. 
This means that HTML elements are affected by too many conflicting CSS declarations and you’ll need to rely on specificity 
to style correctly the element.

### Spacing unit
All margins and paddings must be expressed using a defined variable:
$spacing-unit: XXpx;
Example:
margin-bottom: $spacing-unit*2;

### Color scheme
All colors must be expressed using pre-defined color palette.

## FILE ORGANIZATION
#### Variables
All variables are stored in style/variables.scss file.
Variables must be grouped into logical groups. For variables names, we use kebab-case.
Variables that defines colors will start with color- prefix.

#### Mixins
All mixins are stored in style/mixins.scss file.

#### Global styles
Global styles are declared in App.vue component.

#### Component style
Style for components are defined locally, in a components scoped style element. 
Variables and mixins are already imported globally in every component. The rule for that is set in vue.config.js file.
If you wish to add more globally imported scss files, you need to update this file as well.

## NAMING CONVENTIONS
We use BEM guidelines. 
Block classes are built using a simple hyphen - delimited string; 
Block child elements have classes that are composed by the parent's class plus a custom part delimited using a double underscore __
Modified elements have classes that are composed by the basic element class plus a custom part delimited using a double hyphen.
Example code:

```html
<article class="item-preview">
    <div class="item-preview__thumb-caption-wrapper" >
        <a class="item-preview__thumb" href="...">
            <img class="item-preview__image">
            <p class="item-preview__caption" >
                Caption
            </p>
    </div>
    <div class="item-preview__metadata">
        <a class="item-preview__title-link">
            <h1 class="item-preview__title">
                Title
            </h1>
        </a>
        <h2 class="item-preview__subtitle">
            Subtitle
        </h2>
        <p class="item-preview__description">
            Description
        </p>
    </div>
</article>
```

To avoid repeating the root class prefix in the child classes, please use the Sass Referencing Parent Selector (.&) like this .&__thumb,
.&__title. This solution allows to write less code and, in case the root class is changed, only one line of CSS must be updated. 
The SCSS code for the previously seen component example would be:

```scss
item-preview {
    .&__thumb-caption-wrapper {
        ...
    }
 
    .&__thumb {
        ...
    }
    .&_image {
        ...
    }
 
    .&__caption {
        ...
    }
    ...
 }
```
### Other naming rules
We never rely on HTML tags as CSS selectors: this guarantees that if another developer changes an HTML tag, 
the style of the page is not affected. 
For this reason, each HTML element must have a class.
Name classes based on what they are, not how they look like or what HTML tag they refer to.