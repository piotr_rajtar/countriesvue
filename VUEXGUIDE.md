# Vuex Guide

## Basic
```
The State - The source of truth that drives our app. Components dynamically display the content based on the current state.

The Actions - The possible ways the state could change in reaction to user inputs from the displayed view. Actions can contain asynchronous operations, like http requests. Actions are correct way to trigger mutations.

The Mutations - The only way to actually change state in a Vuex store is by committing a mutation.

The Vuex Store - The store wraps your app's state in a object and gives you access to powerful features and patterns, like mutations and getters. Usualy we keep it in index.ts file.

The Modules - Vuex allows us to divide our store into modules. Each module can contain its own state, mutations, actions, getters, and even nested modules

The Getters - Vuex allows us to define "getters" in the store. You can think of them as computed properties for stores. Like computed properties, a getter's result is cached based on its dependencies, and will only re-evaluate when some of its dependencies have changed.
```

### example of Vuex usage with TypeScript
```
https://blog.logrocket.com/how-to-write-a-vue-js-app-completely-in-typescript/
```

### Vuex-Module-Decorators documentacion
```
https://championswimmer.in/vuex-module-decorators/pages/getting-started.html#define-a-module
```

