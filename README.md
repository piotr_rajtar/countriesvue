# CountriesVue Project

## Project setup
Before packages installation copy the repository on your local machine first:
* select "clone repository..." option from VSC "Welcome" tab and pasting:
```
https://piotr_rajtar@bitbucket.org/piotr_rajtar/countriesvue.git
```
* ...or clone repository by terminal:
```
git clone git@bitbucket.org:piotr_rajtar/countriesvue.git
```
After repository cloning, please install project packages:
```
npm install
```
In case of errors during installation please use below command instead, which will install exactly the same dependencies version used in package-lock.json file.
```
npm ci
```

## Project scripts

### Starting localhost dev environment
```
npm run serve
```
### Starting and using json-server
```
npm install -g json-server
```
```
json-server --watch db.json
```
### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
